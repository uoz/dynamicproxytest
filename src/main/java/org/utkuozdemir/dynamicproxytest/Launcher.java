package org.utkuozdemir.dynamicproxytest;

import java.lang.reflect.Proxy;

/**
 * Created by Utku on 13.7.2014...
 */
public class Launcher {
	public static void main(String[] args) {
		ClassLoader classLoader = Launcher.class.getClassLoader();
		Class<?>[] interfaces = {Test.class};
		TestProxy proxy = new TestProxy(new TestImpl());

		Test test = (Test) Proxy.newProxyInstance(classLoader, interfaces, proxy);
		test.sayHello();

		System.out.println(test.multiply(5, 7));
	}
}