package org.utkuozdemir.dynamicproxytest;

/**
 * Created by Utku on 13.7.2014...
 */
public interface Test {
	public void sayHello();

	public int multiply(int number1, int number2);
}
