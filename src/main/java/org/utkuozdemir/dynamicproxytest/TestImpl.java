package org.utkuozdemir.dynamicproxytest;

/**
 * Created by Utku on 13.7.2014...
 */
public class TestImpl implements Test {
	@Override
	public void sayHello() {
		System.out.println("I AM A METHOD THAT ONLY SAY HELLO, SO HELLO!!");
	}

	@Override
	public int multiply(int number1, int number2) {
		return number1 * number2;
	}
}
