package org.utkuozdemir.dynamicproxytest;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * Created by Utku on 13.7.2014...
 */
public class TestProxy implements InvocationHandler {
	private TestImpl impl;

	public TestProxy(TestImpl impl) {
		this.impl = impl;
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		System.out.println("I AM THE INVOKER, WORK IS STARTING!");
		if (Object.class == method.getDeclaringClass()) {
			String name = method.getName();
			switch (name) {
				case "equals":
					return proxy == args[0];
				case "hashCode":
					return System.identityHashCode(proxy);
				case "toString":
					return proxy.getClass().getName() + "@" +
							Integer.toHexString(System.identityHashCode(proxy)) +
							", with InvocationHandler " + this;
				default:
					throw new IllegalStateException(String.valueOf(method));
			}
		}
		Object invokeResult = method.invoke(impl, args);

		System.out.println("I AM THE INVOKER, WORK IS DONE!");

		return invokeResult;
	}
}
